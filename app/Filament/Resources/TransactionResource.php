<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TransactionResource\Pages;
use App\Models\Transaction;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Hidden;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;

class TransactionResource extends Resource
{
    protected static ?string $model = Transaction::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    protected static ?string $navigationGroup = 'Master Data';

    protected static ?string $navigationLabel = 'Transaksi';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()->schema([
                    Hidden::make('user_id')->default(auth()->user()->id),
                    Select::make('team_id')->relationship('team', 'name'),
                    TextInput::make('location')->required(),
                    TextInput::make('source_ip')->required(),
                    TextInput::make('destination_ip')->required(),
                    TextInput::make('service')->required(),
                    TextInput::make('comment')->required(),
                    TextInput::make('status')
                        ->datalist([
                            'WAITING_MANAGER',
                            'APPROVE_MANAGER',
                            'REJECT_MANAGER',
                            'CANCEL_USER',
                            'DONE',
                        ])
                        ->required()
                        ->default('WAITING_MANAGER'),
                    Hidden::make('role')->default('User'),
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('user.name'),
                TextColumn::make('team.name'),
                TextColumn::make('location'),
                TextColumn::make('source_ip'),
                TextColumn::make('destination_ip'),
                TextColumn::make('service'),
                TextColumn::make('status'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTransactions::route('/'),
            'create' => Pages\CreateTransaction::route('/create'),
            'edit' => Pages\EditTransaction::route('/{record}/edit'),
        ];
    }
}
