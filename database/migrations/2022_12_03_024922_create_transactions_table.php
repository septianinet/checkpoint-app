<?php

use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class, 'user_id');
            $table->foreignIdFor(Team::class, 'team_id');
            $table->string('location');
            $table->string('source_ip');
            $table->string('destination_ip');
            $table->string('service');
            $table->string('comment');
            $table->string('role');
            $table->enum('status', ['WAITING_MANAGER', 'APPROVED_MANAGER', 'REJECT_MANAGER', 'CANCEL_USER', 'DONE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
